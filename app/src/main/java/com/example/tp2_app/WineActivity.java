package com.example.tp2_app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    EditText winename;
    EditText region;
    EditText localisation;
    EditText climat;
    EditText Superface_plat;
    Button sauve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        final WineDbHelper db=new WineDbHelper(getApplicationContext());

        winename=(EditText) findViewById(R.id.wineName);
        region=(EditText) findViewById(R.id.editWineRegion);
        localisation=(EditText) findViewById(R.id.editLoc);
        climat=(EditText) findViewById(R.id.editClimate);
        Superface_plat=(EditText) findViewById(R.id.editPlantedArea);
        sauve=(Button) findViewById(R.id.button);

        Intent intent=getIntent();

      Bundle bun =intent.getBundleExtra("Wine");


       final  Wine wine = (Wine) intent.getParcelableExtra("wine");

       // winename.setText(wine.getTitle());

        winename.setText(wine.getTitle());
        region.setText(wine.getRegion());
        localisation.setText(wine.getLocalization());
        climat.setText(wine.getClimate());
        Superface_plat.setText(wine.getPlantedArea());

        sauve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String winenam=String.valueOf(winename.getText());

                if(winenam.matches("")){
                    AlertDialog.Builder builder= new AlertDialog.Builder(WineActivity.this);
                    builder.setTitle("Impossible de de saugarder");
                    builder.setMessage("le nom de vin ne doit pas etre vide ");
                    builder.setCancelable(true);
                    builder.show();
                    return;


                }

               else {
                    wine.setTitle(String.valueOf(winename.getText()));
                    wine.setRegion(String.valueOf(region.getText()));
                    wine.setLocalization(String.valueOf(localisation.getText()));
                    wine.setClimate(String.valueOf(climat.getText()));
                    wine.setPlantedArea(String.valueOf(Superface_plat.getText()));

                    db.updateWine(wine);

                    Toast.makeText(WineActivity.this, "SAUVEGARDE EFFECTUÉ", Toast.LENGTH_LONG).show();


                }
            }
        });


        //2eime methode


    }

}
